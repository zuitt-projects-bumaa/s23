	db.users.insertMany([
	{
		"firstName" : "Nick",
		"lastName" : "Carter",
		"email" : "nc@mail.com",
		"isAdmin" : false
	},
	{
		"firstName" : "Howie",
		"lastName" : "Dorough",
		"email" : "hd@mail.com",
		"isAdmin" : false
	},
	{
		"firstName" : "AJ",
		"lastName" : "McLean",
		"email" : "am@mail.com",
		"isAdmin" : false
	},
	{
		"firstName" : "Brian",
		"lastName" : "Littrell",
		"email" : "bl@mail.com",
		"isAdmin" : false
	},
	{
		"firstName" : "Kevin",
		"lastName" : "Richardson",
		"email" : "kr@mail.com",
		"isAdmin" : false
	}]);


	db.courses.insertMany([
	{
		"name" : "Javascript 101",
		"price" : 8000,
		"isActive" : false
	},
	{
		"name" : "MongoDB 101",
		"price" : 5000,
		"isActive" : false
	},
	{
		"name" : "React 101",
		"price" : 7300,
		"isActive" : false
	}]);

db.users.find({"isAdmin" : false})

db.courses.find()

//upadate first user to admin
db.users.updateOne({}, {$set: {"isAdmin" : "true"}})

db.courses.updateOne({"name" : "React 101"}, {$set: {"isActive" : "true"}})

db.courses.deleteMany({"isActive" : false})

