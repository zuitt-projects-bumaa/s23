// What is MongoDB Atlas?
	// MongoDB Atlas is a MongoDB database in the cloud. It is the cloud service of MongoDB, the leading noSQL database.

// What is Robo3T?
	// Robo3T is an application which allows the use of GUI to manipulate MongoDB databases. The advantage of Robo3T in just using Atlas is that Robo3T is a local application

// What is Shell?
	// Shell is an interface in MongoDB that allows us to input commands and perform CRUD operations in our database

// CRUD Operations
	// Create, Read, Update and Delete


	// Create- inserting documents
		// insertOne({})
	/*
		db.collection.insertOne({
			"fieldA" : "value A",
			"fieldB" : "value B"

	})- allows us to insert a document into a collection
	*/

	db.users.insertOne({

		"firstName" : "Jean",
		"lastName" : "Smith",
		"age" : 24,
		"contact" : {
			"phone" : "09123456789",
			"email" : "js@mail.com"
		},
		"courses" : [
			"CSS", "Javascript", "ReactJS"
		]

		"department" : "none"
	});

	// insertMany({})
		/*
			Syntax:
				db.collections.insertMany([
					{objectA}, {objectB}
				])

		*/

	db.users.insertMany([
			{
				"firstName" : "Stephen",
				"lastName" : "Hawking",
				"age" : 76,
				"contact" : {
					"phone" : "09123456789",
					"email" : "stephenhawking@mail.com"
				},
				"courses": [
					"Python", "PHP", "React"
				],
				"department" : "none"
			},
			{
				"firstName" : "Neil",
				"lastName" : "Armstrong",
				"age" : 82,
				"contact" : {
					"phone" : "09987654321",
					"email" : "armstrong@gmail.com"
				},
				"courses": [
					"Laravel", "Sass", "Springboot"
				],
				"department" : "none"
			}

		])

	// Read-  find documents
		/*
			Syntax:
			db.collection.find()
				>> will return ALL documents
			db.collection.findOne({})
				>> will return the first document
			db.collection.find({"criteria" : "value"})
				>> will return ALL documents that matches the criteria in the collection
			db.collection.findOne({"criteria" : "value"})
				>> will return the first document that matches the criteria
			db.collection.find({"fieldA" : "valueA", "fieldB" : "valueB"})

		*/

		db.users.find()

		db.users.findOne({})

		db.users.find({"firstName" : "Jean"})

		db.users.findOne({"firstName" : "Kate"})

		db.users.find({"lastName" : "Armstrong", "age" : 82})

	// Update documents
		/*
			Syntax:
				db.collection.updateOne({"criteria" : "value"}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
					>> to update the first item that matches the criteria

				db.collection.updateOne({}, {$set: {"fieldToBeUpdated" : "updatedValue"}})
					>> to update the first item in the collection

				db.collection.updateMany({"criteria" : "value"}, {$set: {"fieldToBeUpdated" : "updatedValue"}})
					>> to update all items that matches the criteria

				db.collection.updateMany({}, {$set: {"fieldToBeUpdated" : "updatedValue"}})
					>> to update all items in the collection
		*/

		db.users.updateOne({"firstName" : "John"}, {$set: {"lastName" : "Gates"}})

		db.users.updateOne({}, {$set: {"emergencyContact" : "father"}})

		db.users.updateMany({"department" : "none"}, {$set: {"department" : "Tech"}})

		db.users.updateMany({"department" : "Tech"}, {$set: {"dept" : "Tech"}})
		db.users.updateOne({}, {$rename: {"dept" : "department"}})

		db.users.updateMany({}, {$set: {"emergencyContact" : "mother"}})


// Delete - to delete documents

/*
Syntax

	db.collection.deleteOne({"criteria" : "value"})
	db.collection.deleteMany({"criteria" : "value"})
	db.collection.deleteOne({})
	db.collection.deleteMany({})
*/

db.users.deleteMany({})